package com.example.provadxc.view;

import com.example.provadxc.model.PhotoModel;

import java.util.List;

public interface SearchView {

    void showLoading();

    void hideLoading();

    void noItemsFound();

    void showItems(List<PhotoModel> photosList, int pageNumber);
}
