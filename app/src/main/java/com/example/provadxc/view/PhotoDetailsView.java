package com.example.provadxc.view;

import com.example.provadxc.model.PhotoDetailsModel;

public interface PhotoDetailsView {

    void setData(PhotoDetailsModel photoDetailsModel);

}
