package com.example.provadxc.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;

import com.example.provadxc.R;
import com.example.provadxc.databinding.MainActivityBinding;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivityBinding mainActivityBinding = MainActivityBinding.inflate(getLayoutInflater());
        Fragment startingFragment = new SearchFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.main_container, startingFragment);
        transaction.commit();
        View view = mainActivityBinding.getRoot();
        setContentView(view);
    }
}