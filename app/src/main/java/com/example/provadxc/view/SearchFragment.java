package com.example.provadxc.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.provadxc.adapter.FoundImagesAdapter;
import com.example.provadxc.databinding.SearchFragmentBinding;
import com.example.provadxc.model.PhotoModel;
import com.example.provadxc.presenter.SearchPresenter;
import com.example.provadxc.presenter.SearchPresenterImpl;

import java.util.ArrayList;
import java.util.List;

public class SearchFragment extends Fragment implements SearchView {
    private SearchFragmentBinding binding;
    private SearchPresenter presenter;
    private FoundImagesAdapter adapter;
    private static final int initialPageNumber = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = SearchFragmentBinding.inflate(inflater, container, false);
        presenter = new SearchPresenterImpl(this);
        View view = binding.getRoot();
        binding.searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (!s.equals("")) {
                    presenter.searchKeywords(binding.searchView.getQuery().toString(), initialPageNumber);
                    binding.notFoundLayout.setVisibility(View.GONE);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        binding.recyclerView.setLayoutManager(layoutManager);
        adapter = new FoundImagesAdapter(new ArrayList<>(), presenter, initialPageNumber);
        binding.recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding.recyclerView.setAdapter(null);
        adapter = null;
        binding = null;
    }

    @Override
    public void showLoading() {
        if (binding != null) {
            binding.progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading() {
        if (binding != null){
            binding.progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void noItemsFound() {
        if (binding != null) {
            binding.notFoundLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showItems(List<PhotoModel> photosList, int pageNumber) {
        adapter = new FoundImagesAdapter(photosList, presenter, pageNumber);
        if (binding != null) {
            binding.notFoundLayout.setVisibility(View.GONE);
            binding.recyclerView.setAdapter(adapter);
        }
    }
}
