package com.example.provadxc.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.provadxc.R;
import com.example.provadxc.databinding.DetailsFragmentBinding;
import com.example.provadxc.model.PhotoDetailsModel;
import com.example.provadxc.model.PhotoModel;
import com.example.provadxc.presenter.PhotoDetailsPresenter;
import com.example.provadxc.presenter.PhotoDetailsPresenterImpl;
import com.example.provadxc.utils.ImageUtils;
import com.squareup.picasso.Picasso;

public class PhotoDetailsFragment extends Fragment implements PhotoDetailsView {

    private static String pictureURL = "https://live.staticflickr.com/%s/%s_%s_c.jpg";
    private DetailsFragmentBinding binding;
    private PhotoModel photoModel;
    private PhotoDetailsPresenter presenter;

    public PhotoDetailsFragment(PhotoModel photoModel) {
        this.photoModel = photoModel;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DetailsFragmentBinding.inflate(inflater, container, false);
        presenter = new PhotoDetailsPresenterImpl(this);
        presenter.getPhotoInfo(photoModel);
        binding.shareImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.detailsImageview.getDrawable() == null) {
                    Toast.makeText(getContext(), "The image has not loaded yet", Toast.LENGTH_LONG).show();
                } else {
                    if (getContext() != null) {
                        ImageUtils.saveImage(binding.detailsImageview.getDrawable(), getContext());
                        ImageUtils.shareImage(getContext());
                    }
                }
            }
        });
        return binding.getRoot();
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void setData(PhotoDetailsModel photoDetailsModel) {
        binding.authorTextview.setText(photoDetailsModel.getAuthor().equals("") ? "No author available" : photoDetailsModel.getAuthor());
        binding.dateTextview.setText(photoDetailsModel.getDate().equals("") ? "No date available" : photoDetailsModel.getDate());
        binding.titleTextview.setText(photoDetailsModel.getTitle().equals("") ? "No title available" : photoDetailsModel.getTitle());
        binding.descriptionTextview.setText(photoDetailsModel.getDescription().equals("") ? "No description available" : photoDetailsModel.getDescription());
        loadImage();
    }

    private void loadImage() {
        binding.progressBarDetails.setVisibility(View.VISIBLE);
        String finalURL = String.format(pictureURL, photoModel.getServer(), photoModel.getId(), photoModel.getSecret());
        Picasso.with(getContext()).load(finalURL).into(binding.detailsImageview, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                binding.progressBarDetails.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                binding.detailsImageview.setImageDrawable(getContext().getResources().getDrawable(R.drawable.image_not_found));
            }
        });
    }
}
