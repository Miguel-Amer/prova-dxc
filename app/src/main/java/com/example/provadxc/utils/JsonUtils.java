package com.example.provadxc.utils;

import com.example.provadxc.model.PhotoDetailsModel;
import com.google.gson.JsonObject;

public class JsonUtils {

    public static String getUsernameFromResponse(JsonObject json) {
        JsonObject usernameObject = json.getAsJsonObject("person").getAsJsonObject("username");
        String username = usernameObject.get("_content").getAsString();
        return username;
    }

    public static PhotoDetailsModel getDetailsFromJson(JsonObject json) {
        PhotoDetailsModel photoDetailsModel = new PhotoDetailsModel();
        JsonObject photoObject = json.getAsJsonObject("photo");
        JsonObject owner = photoObject.getAsJsonObject("owner");
        String username = owner.get("username").getAsString();
        String title = photoObject.getAsJsonObject("title").get("_content").getAsString();
        String description = photoObject.getAsJsonObject("description").get("_content").getAsString();
        String date = photoObject.getAsJsonObject("dates").get("taken").getAsString();
        photoDetailsModel.setAuthor(username);
        photoDetailsModel.setDate(date);
        photoDetailsModel.setDescription(description);
        photoDetailsModel.setTitle(title);
        return photoDetailsModel;
    }

}
