package com.example.provadxc.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.provadxc.R;
import com.example.provadxc.model.PhotoModel;
import com.example.provadxc.presenter.SearchPresenter;
import com.example.provadxc.view.PhotoDetailsFragment;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FoundImagesAdapter extends RecyclerView.Adapter<FoundImagesAdapter.ViewHolder> {

    private static String thumbnailPictureURL = "https://live.staticflickr.com/%s/%s_%s_n.jpg";

    private List<PhotoModel> dataset;
    private int pageNumber;
    private SearchPresenter presenter;

    public FoundImagesAdapter(List<PhotoModel> list, SearchPresenter presenter, int pageNumber) {
        super();
        this.dataset = list;
        this.pageNumber = pageNumber;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout;
        if (viewType == R.layout.list_element) {
            layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_element, parent, false);
        } else {
            layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_element, parent, false);
        }
        return new ViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position == dataset.size()) {
            holder.pageNumber.setText(Integer.toString(pageNumber));
            holder.previousPageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int currentPageNumber = Integer.parseInt(holder.pageNumber.getText().toString());
                    if (currentPageNumber > 1) {
                        currentPageNumber--;
                    }
                    presenter.searchPage(currentPageNumber);
                }
            });
            holder.nextPageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int currentPageNumber = Integer.parseInt(holder.pageNumber.getText().toString());
                    currentPageNumber++;
                    presenter.searchPage(currentPageNumber);
                }
            });
        } else {
            String finalURL = String.format(thumbnailPictureURL, dataset.get(position).getServer(), dataset.get(position).getId(), dataset.get(position).getSecret());
            Picasso.with(holder.image.getContext()).load(finalURL).into(holder.image);
            holder.title.setText(dataset.get(position).getTitle());
            holder.author.setText(dataset.get(position).getUsername());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PhotoDetailsFragment fragment = new PhotoDetailsFragment(dataset.get(position));
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    activity
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(
                                    R.anim.slide_in,
                                    R.anim.fade_out,
                                    R.anim.fade_in,
                                    R.anim.slide_out
                            )
                            .add(R.id.main_container, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            });
        }
    }

    public List<PhotoModel> getDataset() {
        return dataset;
    }

    public void setDataset(List<PhotoModel> dataset) {
        this.dataset = dataset;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == dataset.size()) ? R.layout.pagination_element : R.layout.list_element;
    }

    @Override
    public int getItemCount() {
        return (dataset.size() > 0 ? dataset.size()+1 : dataset.size());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView title;
        public TextView author;
        public TextView pageNumber;
        public ImageView previousPageButton;
        public ImageView nextPageButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.element_image);
            title = itemView.findViewById(R.id.element_title);
            author = itemView.findViewById(R.id.element_author);
            pageNumber = itemView.findViewById(R.id.page_number);
            previousPageButton = itemView.findViewById(R.id.previous_page_imageview);
            nextPageButton = itemView.findViewById(R.id.next_page_imageview);
        }
    }
}
