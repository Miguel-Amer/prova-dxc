package com.example.provadxc.dataprovider;

import com.example.provadxc.api.FlickrService;
import com.example.provadxc.businesslogic.PhotoDetailsBusinessLogic;
import com.example.provadxc.model.PhotoModel;
import com.google.gson.JsonObject;

import java.util.concurrent.TimeUnit;

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PhotoDetailsDataProviderImpl implements PhotoDetailsDataProvider{

    private PhotoDetailsBusinessLogic photoDetailsBusinessLogic;
    private FlickrService service;

    public PhotoDetailsDataProviderImpl(PhotoDetailsBusinessLogic photoDetailsBusinessLogic) {
        this.photoDetailsBusinessLogic = photoDetailsBusinessLogic;
        service = initializeService();
    }

    private FlickrService initializeService() {
        OkHttpClient okHttpClient = getOkHttpClient();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.flickr.com/services/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit.create(FlickrService.class);
    }

    private OkHttpClient getOkHttpClient() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build();
        return okHttpClient;
    }

    @Override
    public void getPhotoDetails(PhotoModel photoModel) {
        Call<JsonObject> details = service.getPhotoInfo(photoModel.id);
        details.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                photoDetailsBusinessLogic.onDetailsResponse(response.body());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }
}
