package com.example.provadxc.dataprovider;

import com.example.provadxc.api.FlickrService;
import com.example.provadxc.businesslogic.SearchBusinessLogic;
import com.example.provadxc.model.PhotoModel;
import com.example.provadxc.model.PhotosSearchModel;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchDataProviderImpl implements SearchDataProvider {


    private SearchBusinessLogic searchBusinessLogic;
    private FlickrService service;

    public SearchDataProviderImpl(SearchBusinessLogic searchBusinessLogic) {
        this.searchBusinessLogic = searchBusinessLogic;
        service = initializeService();
    }

    private FlickrService initializeService() {
        OkHttpClient okHttpClient = getOkHttpClient();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.flickr.com/services/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit.create(FlickrService.class);
    }

    private OkHttpClient getOkHttpClient() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build();
        return okHttpClient;
    }

    @Override
    public void searchByKeywords(String keywords, int pageNumber) {
        Call<PhotosSearchModel> photos = service.searchKeywords(keywords, pageNumber);
        photos.enqueue(new Callback<PhotosSearchModel>() {
            @Override
            public void onResponse(Call<PhotosSearchModel> call, Response<PhotosSearchModel> response) {
                if (response.body().getPhotos().getPhotos().size() > 0) {
                    searchBusinessLogic.getDataFromKeywordResponse(response.body());
                } else {
                    searchBusinessLogic.onErrorResponse();
                }
            }

            @Override
            public void onFailure(Call<PhotosSearchModel> call, Throwable t) {
                searchBusinessLogic.onErrorResponse();
            }
        });
    }

    @Override
    public void searchUsername(ArrayList<PhotoModel> models) {
        List<Observable<?>> requests = new ArrayList<>();
        for (PhotoModel model: models) {
            requests.add(service.searchByUserId(model.getOwner()));
        }
        Observable.zip(requests, new Function<Object[], Object>() {
            @Override
            public Object apply(Object[] objects) throws Throwable {
                return objects;
            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object o) throws Throwable {
                Object[] source = ((Object[])o);
                List<JsonObject> jsonList = new ArrayList<>();
                for(int i = 0; i < source.length; i++) {
                    if (source[i] instanceof JsonObject) {
                        jsonList.add((JsonObject) source[i]);
                    }
                }
                searchBusinessLogic.assignUsername(jsonList);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Throwable {
                searchBusinessLogic.onErrorResponse();
            }
        });

    }

}
