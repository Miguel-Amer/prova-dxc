package com.example.provadxc.dataprovider;

import com.example.provadxc.model.PhotoModel;

import java.util.ArrayList;

public interface SearchDataProvider {

    void searchByKeywords(String keywords, int pageNumber);

    void searchUsername(ArrayList<PhotoModel> model);


}
