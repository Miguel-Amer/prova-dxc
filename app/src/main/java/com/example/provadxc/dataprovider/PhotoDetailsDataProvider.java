package com.example.provadxc.dataprovider;

import com.example.provadxc.model.PhotoModel;

public interface PhotoDetailsDataProvider {

    void getPhotoDetails(PhotoModel photoModel);

}
