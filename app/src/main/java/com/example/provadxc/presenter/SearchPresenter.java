package com.example.provadxc.presenter;

import com.example.provadxc.model.PhotoModel;

import java.util.List;

public interface SearchPresenter {

    void searchKeywords(String keywords, int pageNumber);

    void showPhotos(List<PhotoModel> photosList, int pageNumber);

    void searchPage(int pageNumber);
}
