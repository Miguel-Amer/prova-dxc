package com.example.provadxc.presenter;

import com.example.provadxc.model.PhotoDetailsModel;
import com.example.provadxc.model.PhotoModel;

public interface PhotoDetailsPresenter {

    void getPhotoInfo(PhotoModel photoModel);

    void showPhotoInfo(PhotoDetailsModel photoDetailsModel);

}
