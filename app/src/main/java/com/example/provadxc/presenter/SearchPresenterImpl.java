package com.example.provadxc.presenter;

import com.example.provadxc.businesslogic.SearchBusinessLogic;
import com.example.provadxc.businesslogic.SearchBusinessLogicImpl;
import com.example.provadxc.model.PhotoModel;
import com.example.provadxc.view.SearchView;

import java.util.List;

public class SearchPresenterImpl implements SearchPresenter {

    private SearchBusinessLogic searchBusinessLogic;
    private SearchView searchView;
    private String lastKeywordsSearched = "";

   public SearchPresenterImpl(SearchView searchView) {
       this.searchBusinessLogic = new SearchBusinessLogicImpl(this);
       this.searchView = searchView;
   }

    @Override
    public void searchKeywords(String keywords, int pageNumber) {
        lastKeywordsSearched = keywords;
        searchBusinessLogic.searchKeywords(keywords, pageNumber);
        searchView.showLoading();
    }

    @Override
    public void showPhotos(List<PhotoModel> photosList, int pageNumber) {
       if (photosList == null) {
           searchView.noItemsFound();
           searchView.hideLoading();
           return;
       }
       if (photosList.isEmpty()) {
           searchView.noItemsFound();
       } else {
           searchView.showItems(photosList, pageNumber);
       }
       searchView.hideLoading();
    }

    @Override
    public void searchPage(int pageNumber) {
        searchBusinessLogic.searchKeywords(lastKeywordsSearched, pageNumber);
        searchView.showLoading();
    }
}
