package com.example.provadxc.presenter;

import com.example.provadxc.businesslogic.PhotoDetailsBusinessLogic;
import com.example.provadxc.businesslogic.PhotoDetailsBusinessLogicImpl;
import com.example.provadxc.model.PhotoDetailsModel;
import com.example.provadxc.model.PhotoModel;
import com.example.provadxc.view.PhotoDetailsView;

public class PhotoDetailsPresenterImpl implements PhotoDetailsPresenter {

    private PhotoDetailsView photoDetailsView;
    private PhotoDetailsBusinessLogic photoDetailsBusinessLogic;

    public PhotoDetailsPresenterImpl(PhotoDetailsView photoDetailsView) {
        this.photoDetailsView = photoDetailsView;
        photoDetailsBusinessLogic = new PhotoDetailsBusinessLogicImpl(this);
    }

    @Override
    public void getPhotoInfo(PhotoModel photoModel) {
        photoDetailsBusinessLogic.getPhotoDetails(photoModel);
    }

    @Override
    public void showPhotoInfo(PhotoDetailsModel photoDetailsModel) {
        photoDetailsView.setData(photoDetailsModel);
    }
}
