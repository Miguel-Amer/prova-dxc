package com.example.provadxc.api;

import com.example.provadxc.BuildConfig;
import com.example.provadxc.model.PhotosSearchModel;
import com.google.gson.JsonObject;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FlickrService {

    @GET("rest/?method=flickr.photos.search&api_key="+BuildConfig.FLICKR_API_KEY+"&format=json&nojsoncallback=1&per_page=20")
    Call<PhotosSearchModel> searchKeywords(@Query("tags") String keywords, @Query("page") int pageNumber);

    @GET("rest/?method=flickr.people.getInfo&api_key="+BuildConfig.FLICKR_API_KEY+"&format=json&nojsoncallback=1")
    Observable<JsonObject> searchByUserId(@Query("user_id") String userId);

    @GET("rest/?method=flickr.photos.getInfo&api_key="+BuildConfig.FLICKR_API_KEY+"&format=json&nojsoncallback=1")
    Call<JsonObject> getPhotoInfo(@Query("photo_id") String photoId);

}
