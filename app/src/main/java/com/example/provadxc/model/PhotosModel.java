package com.example.provadxc.model;

import java.util.ArrayList;

public class PhotosModel {

    public Integer page;
    public Integer pages;
    public Integer perpage;
    public Integer total;
    public ArrayList<PhotoModel> photo;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public Integer getPerpage() {
        return perpage;
    }

    public void setPerpage(Integer perpage) {
        this.perpage = perpage;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public ArrayList<PhotoModel> getPhotos() {
        return photo;
    }

    public void setPhoto(ArrayList<PhotoModel> photo) {
        this.photo = photo;
    }
}
