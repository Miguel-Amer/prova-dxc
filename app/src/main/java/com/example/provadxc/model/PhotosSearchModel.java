package com.example.provadxc.model;

public class PhotosSearchModel {

    public PhotosModel photos;
    public String stat;

    public PhotosModel getPhotos() {
        return photos;
    }

    public void setPhotos(PhotosModel photos) {
        this.photos = photos;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }
}
