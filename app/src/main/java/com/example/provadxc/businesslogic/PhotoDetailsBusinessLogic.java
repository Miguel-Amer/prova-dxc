package com.example.provadxc.businesslogic;

import com.example.provadxc.model.PhotoModel;
import com.google.gson.JsonObject;

public interface PhotoDetailsBusinessLogic {

    void getPhotoDetails(PhotoModel photoModel);

    void onDetailsResponse(JsonObject json);

}
