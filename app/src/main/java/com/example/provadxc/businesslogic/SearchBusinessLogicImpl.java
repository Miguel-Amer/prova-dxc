package com.example.provadxc.businesslogic;

import com.example.provadxc.dataprovider.SearchDataProvider;
import com.example.provadxc.dataprovider.SearchDataProviderImpl;
import com.example.provadxc.model.PhotoModel;
import com.example.provadxc.model.PhotosModel;
import com.example.provadxc.model.PhotosSearchModel;
import com.example.provadxc.presenter.SearchPresenter;
import com.example.provadxc.utils.JsonUtils;
import com.google.gson.JsonObject;

import java.util.List;

public class SearchBusinessLogicImpl implements SearchBusinessLogic {

    private SearchPresenter searchPresenter;
    private SearchDataProvider searchDataProvider;
    private List<PhotoModel> photoModelList;
    private int pageNumber = 0;

    public SearchBusinessLogicImpl(SearchPresenter searchPresenter) {
        this.searchPresenter = searchPresenter;
        this.searchDataProvider = new SearchDataProviderImpl(this);
    }

    @Override
    public void searchKeywords(String keywords, int pageNumber) {
        searchDataProvider.searchByKeywords(keywords, pageNumber);
    }

    @Override
    public void getDataFromKeywordResponse(PhotosSearchModel photosSearchModel) {
        PhotosModel photosModel= photosSearchModel.getPhotos();
        photoModelList = photosModel.getPhotos();
        pageNumber = photosModel.getPage();
        searchDataProvider.searchUsername(photosModel.getPhotos());
    }

    @Override
    public void assignUsername(List<JsonObject> jsonList) {
        if (jsonList != null) {
            if (!jsonList.isEmpty()) {
                for (int i = 0; i < photoModelList.size(); i++) {
                    photoModelList.get(i).setUsername(JsonUtils.getUsernameFromResponse(jsonList.get(i)));
                }
            }
        }
        searchPresenter.showPhotos(photoModelList, pageNumber);
    }

    @Override
    public void onErrorResponse() {
        searchPresenter.showPhotos(null, pageNumber);
    }


}
