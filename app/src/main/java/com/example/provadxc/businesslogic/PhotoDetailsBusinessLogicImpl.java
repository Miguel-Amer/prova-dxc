package com.example.provadxc.businesslogic;

import com.example.provadxc.dataprovider.PhotoDetailsDataProvider;
import com.example.provadxc.dataprovider.PhotoDetailsDataProviderImpl;
import com.example.provadxc.model.PhotoDetailsModel;
import com.example.provadxc.model.PhotoModel;
import com.example.provadxc.presenter.PhotoDetailsPresenter;
import com.example.provadxc.utils.JsonUtils;
import com.google.gson.JsonObject;

public class PhotoDetailsBusinessLogicImpl implements PhotoDetailsBusinessLogic{

    private PhotoDetailsPresenter photoDetailsPresenter;
    private PhotoDetailsDataProvider photoDetailsDataProvider;

    public PhotoDetailsBusinessLogicImpl(PhotoDetailsPresenter photoDetailsPresenter) {
        this.photoDetailsPresenter = photoDetailsPresenter;
        photoDetailsDataProvider = new PhotoDetailsDataProviderImpl(this);
    }

    @Override
    public void getPhotoDetails(PhotoModel photoModel) {
        photoDetailsDataProvider.getPhotoDetails(photoModel);
    }

    @Override
    public void onDetailsResponse(JsonObject json) {
        PhotoDetailsModel photoDetailsModel = JsonUtils.getDetailsFromJson(json);
        photoDetailsPresenter.showPhotoInfo(photoDetailsModel);
    }
}
