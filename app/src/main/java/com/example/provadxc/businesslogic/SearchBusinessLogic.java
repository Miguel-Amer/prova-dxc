package com.example.provadxc.businesslogic;

import com.example.provadxc.model.PhotosSearchModel;
import com.google.gson.JsonObject;

import java.util.List;

public interface SearchBusinessLogic {

    void searchKeywords(String keywords, int pageNumber);

    void getDataFromKeywordResponse(PhotosSearchModel photosSearchModel);

    void assignUsername(List<JsonObject> jsonBody);

    void onErrorResponse();
}
